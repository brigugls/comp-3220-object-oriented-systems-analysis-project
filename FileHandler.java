import java.io.File;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


public class FileHandler {
	
	//Field Values Sent From Other Classes 
	String fileName;		//GUI	
	String wantedField;		//GUI
	
	//Field Values Sent to Other Classes
	String[] finalOriginalRecords = new String[10000];	//GUI
	String[] finalMatchingRecords = new String[1000];	//GUI
	String[] finalSortedRecords = new String[10000];	//GUI
	
	//Fields Used Only in This Class
	Scanner fis;				
	String currentRecord;
	File file;
	
	
	/*
	 * Author:Brett Shepley
	 * Name:Display SpreadSheet Function
	 * Type: String[]
	 * Description: This function will take the given file as its input stream.
	 * 				Once confirmed that the file is able to be opened the contents 
	 * 				will then begin to be copied row by row to an ArrayList. 
	 * 				After being copied the contents will then be transfered 
	 * 				to a String[] and returned to the User. 
	 * */
	public String[] dss(String fileName) 
	{
		//Holds Original Records--->Transfer Contents to originalRecords
		ArrayList<String> originalRecordList = new ArrayList<String>();
		this.file = new File(fileName);
		
		try 
		{
			fis = new Scanner(file);						//Creating a Scanner that reads from file
			
			while(fis.hasNext()) 
			{				
				currentRecord = fis.next(); 				//Setting the Current Record
				originalRecordList.add(currentRecord);		//Saving the Current Record in the originalRecordList
			}
			fis.close();									//Closing Stream to avoid any exceptions
		}
		catch(Exception e)
		{
			System.out.println("dss Error "+e);
		}
		
		String[] originalRecords = new String[originalRecordList.size()];		//originalRecords[] created with the same size of the originalRecordList
		originalRecordList.toArray(originalRecords);							//Adding contents of originalRecordList --> originalRecords[]
		return originalRecords;													//Returning the originalRecords[]
	}
	
	/*
	 * Author:Brett Shepley
	 * Name: Specific Field Search Function
	 * Type: String[]
	 * Description: This function allows the user to input a String. 
	 * 				Once the String has been obtained the file will be searched
	 * 				until the end of the file has been reached. 
	 * 				Saving all matching records and storing them in a list.
	 * 				The List then transfers contents into a String[] which
	 * 				gets returned.
	 * */
	public String[] sfs(String searchTerm, String fileName) 
	{
		//Holds Matching Records--->Transfer Contents to matchingRecords[]
		ArrayList<String> matchingRecordList = new ArrayList<String>();
		
		this.file = new File(fileName);
		//Holds cell value to be compared
		String matchField;
		//Flag Used to Trigger Failure Message
		Boolean found = false;
		
		try 
		{
			fis = new Scanner(file);					//Creating a Scanner that reads from file
			fis.next();
			fis.useDelimiter("[,\n]");					//Breaking SpreadSheet rows into separate cells
			
			while(fis.hasNext()) 
			{			
				matchField = fis.next(); 				//Cell to be compared to wantedField			
			
				if(matchField.equalsIgnoreCase(searchTerm))
				{
					currentRecord = ""+matchField+" "+fis.next()+" "+fis.next();		//Create record with the matching cell and accompanied cells
					matchingRecordList.add(currentRecord);								//Adding the found record to the recordList
					found=true;															//Failure Message off
				}
			}
			fis.close();								//Closing Stream to avoid any exceptions
		}
		catch(Exception e)
		{
			System.out.println("Error");
		}
		
		if(!found)
		{	
			currentRecord = "No Matching Record Found";							//Adding Failure message to the matchingRecordList
			matchingRecordList.add(currentRecord);								//Adding the found record to the recordList
		}
		
		String[] matchingRecords = new String[matchingRecordList.size()];		//matchingRecords[] created with the same size of the matchingRecordList
		matchingRecordList.toArray(matchingRecords);							//Adding contents of matchingRecordList to matchingRecords[]
		return matchingRecords;													//Returning the matchingRecords[]
	}
	
	/*
	 * Author:Brett Shepley
	 * Name: SpreadSheet Field Sorting Function
	 * Type: String[]
	 * Description: This function allows the user to input whether to sort in 
	 * 				alphabetical order. The file is then saved to 
	 * 				sortedRecordList which is then sorted using bubble sort
	 * 				since speed isn't an issue. After the sorting is done the
	 * 				contents of sortedRecordList are then copied to sortedRecords[].
	 * 				sortedRecords[] is then returned.
	 */
	
	public String[] ssfs(String fileName) 
	{
		//Holds Matching Records--->Transfer Contents to matchingRecords[]
		ArrayList<String> sortedRecordList = new ArrayList<String>();
			
		this.file = new File(fileName);
		//Holds Value during swap process
		String temp; 
		
		try 
		{
			fis = new Scanner(file);					//Creating a Scanner that reads from file
			
			fis.next();
			while(fis.hasNext()) 
			{			
				sortedRecordList.add(fis.next());		//Adding the found record to the recordList
			}
			fis.close();								//Closing Stream to avoid any exceptions
		}
		catch(Exception e)
		{
			System.out.println("Error");
		}
		
		String[] sortedRecords = new String[sortedRecordList.size()];	//matchingRecords[] created with the same size of the matchingRecordList
		sortedRecordList.toArray(sortedRecords);						//Adding contents of matchingRecordList to matchingRecords[]
		
		for(int j=0;j<sortedRecords.length;j++)
		{
			for(int i = j + 1; i < sortedRecords.length; i++)
			{
				if(sortedRecords[i].compareTo(sortedRecords[j]) < 0)	//Using bubble sort since time complexity isn't of high priority	
				{
					temp = sortedRecords[j];
					sortedRecords[j] = sortedRecords[i];
					sortedRecords[i] = temp;
				}
			}
		}
		return sortedRecords;
	}
}
