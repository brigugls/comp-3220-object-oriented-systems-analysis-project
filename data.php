<?php

session_start();



// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

//database connection
  require_once 'config.php';
  $conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME); 
  if ($conn->connect_error) die($conn->connect_error);


?>


<!DOCTYPE html>
<!-- screen scaling and button style sheet for consitent look across devices-->
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="../css/table.css?v=1.1">
    <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
    <link rel="stylesheet" href="../css/page.css?v=1v1">
    <style type="text/css">
        body{ font: 14px sans-serif; text-align: center; background-color: #82A3CF;  } //#0A9EB5
    </style>
</head>
<body>
    <div class="page-header">
        <h1>Welcome</h1>
    </div>
    <p>

<!-- navigation drop down in top left panel and user permission level/logout functionality in top right panel-->
<script src="../js/dropdown.js"></script>
<div class="dropdown">
  <button onclick="myFunction()" class="dropbtn">Menu</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="welcome.php?a=home">Home</a>
    <a href="data.php?a=books">Data</a>
    <a href="admindash.php?a=admin">Staff Only</a> 
  </div>
</div>

        <a class = "permLevel"> <?php echo htmlspecialchars($_SESSION["level"]) ; ?> </a> 
        <a class = "userButton2" href="reset-password.php" class="btn btn-warning"><button>Reset Password</button></a>
        <br>
        <a class = "userButton1" href="logout.php" class="btn btn-danger"><button>Sign Out</button></a>
    </p>
</body>
</html>

<?php


//This section filters the table by checking for variables passed by the input feilds on the page, on inital load each data
//record shows since no filter values have been passed
  $name = get_post($conn, 'name');
  ucfirst('$name'); 
  $id = get_post($conn, 'rowid');
  $position = get_post($conn, 'position');
  ucfirst('$position');  
  $query  = "SELECT * FROM data WHERE name like '%$name%' AND rowid like '%$id%' AND position like '%$position%'"; 
  
//displatying the correct records from the database
    $result = $conn->query($query);
    if (!$result) die ("Database access failed: " . $conn->error);
    $rows = $result->num_rows;
    
// form to enter the information you wish to filter by
 echo '<br>';
 echo '<form action="data.php" method="post">';
 echo '<input type="text" name="name" id="name" placeholder="Enter Name Here.." value="">';
 echo '<input type="text" name="rowid" id="rowid" placeholder="Enter ID Here.." value="">';
 echo '<input type="text" name="position" id="position" placeholder="Enter Position Here.." value="">';
 echo '<input class="deletebut" type="submit" name="filter1" value="Filter">';

//fetches each record from the databse 
  for ($j = 0 ; $j < $rows ; ++$j)
  {
    $result->data_seek($j);
    $row = $result->fetch_array(MYSQLI_NUM);

// formats the information passed by filter into a table 
echo '<div class = "tablehead">';
echo '<body> <div id="table_ajax"> <table id="myTable"> <thead> <tr> <th>';
echo 'Name </th>  <th> ID</th>  <th> Position </th> </tr></thead> <tbody> </div>';
for ($j=0 ; $j<$rows ; ++$j)
  { echo '<tr>';
    $result->data_seek($j);
    $row = $result->fetch_array(MYSQLI_ASSOC);
    echo '<td>'.$row['name'].'</td>';
    echo '<td>'.$row['rowid'].'</td>';
    echo '<td>'.$row['position'].'</td>';
    echo '</tr>';
  }
 echo '</div> </tbody> </table>';

  }

 echo '</form>';

//close connection to database 
  $result->close();
  $conn->close();
  
  function get_post($conn, $var)
  {
    return $conn->real_escape_string($_POST[$var]);
  }


?>