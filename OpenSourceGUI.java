import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class OpenSourceGUI extends JApplet implements ActionListener
{
	//Field Values Sent From Other Class
	String[] finalOriginalRecords = new String[10000];	//FileHandler
	String[] finalMatchingRecords = new String[1000];	//FileHandler
	String[] finalSortedRecords = new String[10000];	//FileHandler
	
	//Field Values Sent to Other Class
	String fileName;		//FileHandler	
	String wantedField;		//FileHandler
	
	//Fields Used Only in This Class
	JPanel menuPanel;			//Main Display Screen

	Boolean sortedUP=false;		//flag
	Boolean filteredUP=false;	//flag
	Boolean originalUP=false;	//flag
	
	//All Components added to menuPanel
	JTextField fileTextField = new JTextField();						//Text box to Enter FileName/Path
	JTextField wantedFieldTextField = new JTextField();						//Text box to Enter FileName/Path
	
	JLabel fileLabel = new JLabel("Enter .csv File Below");				//Label to point out the fileTextField
	JLabel webpageLabel = new JLabel("Open Source Handler");			//Label to Welcome User to the Applet
	JLabel wantedFieldLabel = new JLabel("Enter Filter Word Below");	//Label to point out the wantedFieldTextField
	
	JButton displayOriginal = new JButton("Display Original");			//JButton to control all original spreadsheet viewing
	JButton displaySorted = new JButton("Display Sorted");				//JButton to control all sorted spreadsheet viewing
	JButton displayFiltered = new JButton("Display Filtered");			//JButton to control all filtered spreadsheet viewing
	
	JList originalList;				//List to hold the original spreadsheet and to be displayed on the menuPanel
	JList sortedList;				//List to hold the sorted spreadsheet and to be displayed on the menuPanel
	JList filteredList;				//List to hold the filtered spreadsheet and to be displayed on the menuPanel
	
	FileHandler obj = new FileHandler();	//Creating FileHandler object to have access to the functions and variables
	
	/*
	 * Author:Brett Shepley
	 * Name: Default Constructor
	 * Type: void
	 * Description: This constructors only purpose
	 * 				is to add the menuPanel to the
	 * 				Applet container.
	 * */
	public OpenSourceGUI()
	{
		try 
		{
			add(msp());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	/*
	 * Author:Brett Shepley
	 * Name:Menu Select Panel Function
	 * Type:JPanel
	 * Description: This function will create a JPanel which will
	 * 				have all desired fields. This JFrame will be 
	 * 				holding all of the later created JPanels. 	
	 * */
	public JPanel msp() throws Exception
	{
		
		menuPanel = new JPanel();
		
		//Creating the Panel
		menuPanel.setBackground(Color.LIGHT_GRAY);
		menuPanel.setSize(new Dimension(800,450));
		menuPanel.setLayout(null);
		
		//Display Original Button SetUp
		displayOriginal.setBounds(325, 110, 150, 30);
		displayOriginal.addActionListener(this);
		//menuPanel.add(displayOriginal);
		
		//Display Sorted Button SetUp
		displaySorted.setBounds(325, 150, 150, 30);
		displaySorted.addActionListener(this);
		//menuPanel.add(displaySorted);
		
		//Display Filtered Button SetUp
		displayFiltered.setBounds(325, 190, 150, 30);
		displayFiltered.addActionListener(this);
		//menuPanel.add(displayFiltered);
		
		//Changing JLabel Font, Type and Size
		webpageLabel.setFont(new Font("Serif", Font.BOLD, 30));
		fileLabel.setFont(new Font("Serif", Font.BOLD, 20));
		wantedFieldLabel.setFont(new Font("Serif", Font.BOLD, 15));
		
		//fileTextField SetUp
		fileTextField.setBounds(350,70,100,30);
		fileTextField.addActionListener(this);
		menuPanel.add(fileTextField);
		
		//fileLabel SetUp
		fileLabel.setBounds(320,40,200,30);
		menuPanel.add(fileLabel);
		
		//webpageLabel SetUp
		webpageLabel.setBounds(275, 3, 300, 35);
		menuPanel.add(webpageLabel);
		
		//wantedFieldLabel SetUp
		wantedFieldLabel.setBounds(320,220,200,30);
		
		//fileTextField SetUp
		wantedFieldTextField.setBounds(350,250,100,30);
		wantedFieldTextField.addActionListener(this);
		
		
		//Setting Panel to Visible
		menuPanel.setVisible(true);
		
		return menuPanel;
	}

	
	/*
	 * Author:Brett Shepley
	 * Name:Action Performed Function
	 * Type:voif
	 * Description: This function handles all of the JComponents actions.
	 * 				When the buttons are pressed the
	 * 				corresponding list is displayed. For the
	 * 				Filtered and Sorted only one can be 
	 * 				displayed at a time, they switch visibility. 	
	 * */
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==displayOriginal)			//Display Original Button Action
		{
			originalUP=true;						//Setting Flag == True
			finalOriginalRecords=obj.dss(fileName);			//Obtaining Original Records from FileHandler Class
			originalList = new JList(finalOriginalRecords);		//Adding finalOriginalRecords to a JList to be displayed
			originalList.setBounds(10, 100, 200, 270);			//Setting Position and Measurements 
			originalList.setVisible(true);						//Making the JList visible
			menuPanel.add(originalList);						//Adding List to menuPanel to be displayed
			menuPanel.repaint();								//Refreshing the menuPanel so JList will be displayed 
		}
		else if(e.getSource()==displaySorted)		//Display Sorted Button Action
		{
			if(filteredUP)
			{
				menuPanel.remove(filteredList);		//Remove filteredList if filterUp == true
				filteredUP=false;					//Setting Flag == false
			}
			sortedUP=true;							//Setting Flag == True
			finalSortedRecords=obj.ssfs(fileName);				//Obtaining Sorted Records from FileHandler Class
			sortedList = new JList(finalSortedRecords);		//Adding finalSortedRecords to a JList to be displayed
			sortedList.setBounds(585, 100, 200, 270);		//Setting Position and Measurements 
			sortedList.setVisible(true);					//Making the JList visible
			menuPanel.add(sortedList);						//Adding List to menuPanel to be displayed
			menuPanel.repaint();							//Refreshing the menuPanel so JList will be displayed 
		}
		else if(e.getSource()==displayFiltered)		//Display Filtered Button Action
		{
			if(sortedUP)
			{
				menuPanel.remove(sortedList);		//Remove sortedList if sortedUp == true
				sortedUP=false;						//Setting Flag == false
			}
			menuPanel.add(wantedFieldLabel);
			menuPanel.add(wantedFieldTextField);
			filteredUP=true;						//Setting Flag == True
			menuPanel.repaint();
		}
		else if(e.getSource()==fileTextField) 
		{
			this.fileName=fileTextField.getText();	//Obtaining FileName/FilePath 
			menuPanel.add(displayOriginal);			//Add Display Original Button after File is loaded
			menuPanel.add(displaySorted);			//Add Sorted Original Button after File is loaded
			menuPanel.add(displayFiltered);			//Add Filtered Original Button after File is loaded
			menuPanel.repaint();
			
		}
		else if(e.getSource()==wantedFieldTextField)
		{
			finalMatchingRecords=obj.sfs(wantedFieldTextField.getText(),fileName);		//Obtaining Filtered Records from FileHandler Class
			filteredList = new JList(finalMatchingRecords);								//Adding finalFilteredRecords to a JList to be displayed
			filteredList.setBounds(585, 100, 200, 270);									//Setting Position and Measurements 
			filteredList.setVisible(true);												//Making the JList visible
			menuPanel.add(filteredList);												//Adding List to menuPanel to be displayed
			menuPanel.repaint();														//Refreshing the menuPanel so JList will be displayed 
		}
	}
}
